import React, { Component } from 'react'
import TableComponent from './TableComponent';

export default class InputField extends Component {
  constructor() {
    super();
    this.state = {
      userInfo: [
        { id: 1, email: "example@gmail.com", username: "username", age: 18, status: "Pending"},
      ],
      newId: 0,
      newEmail: "",
      newUsername: "",
      newAge: "",
    };
  }

  handleInsertEmail = (emailEvent) => {
    this.setState({
      newEmail: emailEvent.target.value,
    },
      () => console.log(this.state.newEmail)
    );
  };

  handleInsertUsername = (usernameEvent) => {
    this.setState({
      newUsername: usernameEvent.target.value,
    },
      () => console.log(this.state.newUsername)
    );
  };

  handleInsertAge = (ageEvent) => {
    this.setState({
      newAge: ageEvent.target.value,
    },
    () => console.log(this.state.newUsername)
    );
  };

  changeStatus = (id) => {
    this.state.userInfo.map(user => {
      if(user.id === id){
        user.status === "Pending" ? (user.status = "Done") : (user.status = "Pending")

      //   this.setState(user.status = "Done")
      //   // user.status = "Done"
      // } 
      // else if (user.id === id && user.status === "Done") {
      //   user.status = "Pending"
      }
  })
  this.setState({user:this.state.userInfo})
    // this.setState({this.state.status = this.props.status}) 
  };

  componentDidUpdate(prevState){
    
  }

  handleSubmit = () => {
    
    const newList = {
      id: this.state.userInfo.length + 1, email: this.state.newEmail, username: this.state.newUsername, age: this.state.newAge, status: "Pending"
    };
    this.setState({
      userInfo: [...this.state.userInfo, newList],
      newId: 0,
      newEmail: "",
      newUsername: "",
      newAge: "",
    },
    () => console.log(this.state.userInfo)
    )
  }


  render() {
    return (
      <div className='flex flex-col w-full items-start'>
        <label htmlFor="email" className='font-semibold text-lg'>{this.props.label1}</label>
        {/* row */}
        <div className='flex rounded w-full items-center py-2 text-center'>
          <span className='w-9 bg-white rounded-l py-2'>✉️</span>
          <input className='rounded-r px-2 w-full py-2 border-none' value={this.state.newEmail} type="email" placeholder='example@gmail.com' onChange={this.handleInsertEmail} />
        </div>
        <label htmlFor="email" className='font-semibold text-lg'>{this.props.label2}</label>
        {/* row */}
        <div className='flex rounded w-full items-center py-2 text-center'>
          <span className='w-9 bg-gray-400 rounded-l py-2'>@</span>
          <input className='rounded-r px-2 w-full py-2 border-none' value={this.state.newUsername} type="text" placeholder='Yeaktong' onChange={this.handleInsertUsername} />
        </div>
        <label htmlFor="email" className='font-semibold text-lg'>{this.props.label3}</label>
        {/* row */}
        <div className='flex rounded w-full items-center py-2 text-center'>
          <span className='w-9 bg-gray-400 rounded-l py-2'>❤️</span>
          <input className='rounded-r px-2 w-full py-2 border-none' value={this.state.newAge} type="number" placeholder='18' onChange={this.handleInsertAge} />
        </div>


        <div className='self-center mt-4 p-0.5 rounded-lg bg-gradient-to-r from-myBlue-500 to-myRed-500 w-40'>
          <button className='bg-white font-bold p-2 rounded-lg hover:text-white hover:bg-gradient-to-r from-myBlue-500 to-myRed-500 w-full shadow-[0_4px_9px_-4px_#14a44d]' onClick={this.handleSubmit}>Register</button>
        </div>

        <TableComponent users = {this.state.userInfo} changeStatus={this.changeStatus}/>
      </div>
    )
  }
}
