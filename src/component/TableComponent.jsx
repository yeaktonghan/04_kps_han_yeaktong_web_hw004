import React, { Component } from "react";
import Swal from 'sweetalert2';
import 'animate.css';

export default class TableComponent extends Component {

  isEven = (idx) => idx % 2 === 0;

  checkStatus = (status) => status === "Pending";

  onClickAlert = (data) => {
    Swal.fire({
      confirmButtonColor: '#519849',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      },
        title: "ID: "+ data.id 
         +"\n Email: "+ (data.email === "" ? "null" : data.email)
         +"\n UserName: "+ (data.username === "" ? "null" : data.username)
         +"\n Age: "+(data.age === "" ? "null" : data.age),
        
     })
  }


  // handleButtonPress = (user) => {
  //   // console.log(user)
  //   // console.log(user.id) 
  //   this.props.users.map(eUser => {
  //     if(eUser.id ==== user){
  //       console.log(eUser.id)
  //       console.log(user)
  //     }
  //   })
    
  // }

componentDidUpdate(prevProps) {
  if (this.props.users !== prevProps.users) {
    // this.fetchData(this.props.users);
  }
}
    

  render() {
    return (
      <div className="container flex justify-center pt-6">
        <table className="w-full text-center">
          <thead>
            <tr className="text-lg font-black">
              <th className="px-9">ID</th>
              <th>Email</th>
              <th>Username</th>
              <th>Age</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users.map((user, idx) => (
              <tr>
                <td className={this.isEven(idx) ? "bg-white" : "bg-red-300"}>{user.id}</td>
                <td className={this.isEven(idx) ? "bg-white" : "bg-red-300"}>{user.email === "" ? "null" : user.email}</td>
                <td className={this.isEven(idx) ? "bg-white" : "bg-red-300"}>{user.username  === "" ? "null" : user.username}</td>
                <td className={this.isEven(idx) ? "bg-white" : "bg-red-300"}>{user.age  === "" ? "null" : user.age}</td>
                <td className={this.isEven(idx) ? "bg-white" : "bg-red-300"}>
                  <button onClick={() => {this.props.changeStatus(user.id)}} className={this.checkStatus(user.status) ? "bg-red-700 hover:bg-red-900 p-2 my-1 rounded-lg w-28 text-white" : "bg-green-700 hover:bg-green-900 p-2 rounded-lg w-28 text-white"}>
                    {user.status}
                  </button>
                  &nbsp;
                  <button className="bg-blue-700 hover:bg-blue-900 p-2 rounded-lg w-44 my-1 text-white" onClick={() => this.onClickAlert(user)}>
                    show more
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
