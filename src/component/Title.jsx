import React, { Component } from 'react'

export default class Title extends Component {
  render() {
    return (
      <div className='flex justify-center'>
        <div className='flex text-4xl font-bold p-9'>
        <h1 className='bg-clip-text bg-auto text-transparent bg-gradient-to-r from-myBlue-500 to-myRed-500 py-3'>{this.props.title1}</h1>
        <h1 className='py-3'>&nbsp;{this.props.title2}</h1>
        </div>
      </div>
    )
  }
}
