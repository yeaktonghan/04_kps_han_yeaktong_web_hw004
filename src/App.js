import './App.css';
import Title from './component/Title';
import InputField from './component/InputField';

function App() {
  return (
    <div className="container mx-auto">
        <Title title1 = "Please fill in your" title2 = "information"/>
        <InputField label1 = "Your Email" label2 = "Username" label3 = "Age"/>
    </div>
  );
}

export default App;
