/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors:{
        myBlue: {
          500: '#3fb9f4',
        },
        myRed: {
          500: '#d4246c',
        },
      },
    },
  },
  plugins: [],
}
